#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-only
# This file is part of renspell.

from pathlib import Path
from typing import List

from renspell.parse import ParsedLine, parse_file


def test_parse_the_question() -> None:
    parsed: List[ParsedLine] = parse_file(Path("tests/the-question.rpy"))

    assert len(parsed) == 75
    assert (
        parsed[0].content
        == "It's only when I hear the sounds of shuffling feet and supplies being put away that I realize that the lecture's over."
    )
    assert parsed[0].filename == "tests/the-question.rpy"
    assert parsed[0].line_number == 25
